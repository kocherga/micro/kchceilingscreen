# Kocherga Ceiling Light

That creepy thing in the ceiling at coffeepoint.
Uses es8266 12e (nodemcuv2) and a huge Neopixel array.

Available MQTT topics:

`ceiling_display/set` — takes 32\*32\*3 byte image file with BGR values in rather strange order and displays it. See `convimg.py` for details.
```bash
mosquitto_pub -t ceiling_display/set -f image.bin
```

`ceiling_display/ch_brightness` — Takes 1 byte and sets maximum brightness to that value.
```bash
mosquitto_pub -t ceiling_display/ch_brightness -m $'\x01'
```

