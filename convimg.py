#!/usr/bin/env python3.7
from io import BufferedWriter

from PIL import Image, ImageFile
import sys
import struct
import argparse

# these define our display configuration

# display size
w = 32
h = 32
# region size
rw = 16
rh = 16
# region positions and transforms
transforms = [
    # offset x, offset y, flip x, flip y
    (0, 1, 0, 1), (1, 1, 0, 1), (1, 0, 1, 0), (0, 0, 1, 0)
]

parser = argparse.ArgumentParser(description="Converts image into format that ceiling screen in Kocherga supports")
parser.add_argument("-i", help="Source image file", required=True, type=argparse.FileType("rb"))
parser.add_argument("-o", help="Dest image file", default=sys.stdout, type=argparse.FileType("wb"))
args = parser.parse_args(sys.argv[1:])

img: ImageFile = Image.open(args.i).resize((w, h)).convert(mode="RGB")


def curve_to_coord(i: int):
    regsize = rw * rh
    tf = transforms[i // regsize]
    i = (i % regsize)

    x = i // rh
    y = i % rh

    offx, offy, flipx, flipy = tf

    # flipping y if row is odd
    flipy ^= (x % 2 == 0)

    x = rw - 1 - x if flipx else x
    y = rh - 1 - y if flipy else y

    x += offx * rw
    y += offy * rh

    return x, y


def conv_image():
    for index in range(w * h):
        coord = curve_to_coord(index)
        yield img.getpixel(coord)


def simple_encode():
    arr = bytearray([0])
    for px in conv_image():
        arr.append(px[2])
        arr.append(px[1])
        arr.append(px[0])
    return arr


def palette_encode():
    arr = bytearray([1])

    palette = list(set(img.getdata()))

    def lconv():
        return map(lambda a: palette.index(a), conv_image())

    print(f"color count: {len(palette)}")
    arr.append(len(palette) - 1)
    for i in palette:
        r, g, b = i
        arr.append(b)
        arr.append(g)
        arr.append(r)
    print(f"prefix and palette {len(arr)}")

    prev_color = -1
    acc = 0
    pxnum_abs = 0
    num_abs = 0
    data_size = 0

    def drain_acc():
        nonlocal acc
        nonlocal pxnum_abs
        nonlocal data_size
        cindex = palette.index(prev_color) + 1
        if acc > 2:
            arr.append(0)
            arr.append(acc)
            arr.append(cindex)
            data_size += 3
        if acc == 2:
            arr.append(cindex)
            arr.append(cindex)
            data_size += 2
        if acc == 1:
            arr.append(cindex)
            data_size += 1
        pxnum_abs += acc
        acc = 0

    for px in conv_image():
        if prev_color == px and acc < 255:
            pass
        else:
            if acc > 0:
                drain_acc()
        acc += 1
        prev_color = px
        num_abs += 1

    drain_acc()

    print(f"{pxnum_abs}/{num_abs}, data section size: {data_size}, complete size: {len(arr)}")

    return arr


if len(set(img.getdata())) <= 256:
    print("encoding with palette algo")
    args.o.write(palette_encode())
else:
    print("encoding with simple algo")
    args.o.write(simple_encode())
