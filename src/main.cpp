#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Adafruit_NeoPixel.h>
#include <PubSubClient.h>
#include <stdint.h>
// #include "logos.h"


// Which pin on the Arduino is connected to the NeoPixels?
#define PIN            D6
// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      32 * 32
// bytes per input color
#define BPC            3

#define DEBUG_BAUD_RATE     115200

#define MQTT_ID                  "ceiling_display"
#define MQTT_TOPIC               "ceiling_display/#"
#define MQTT_TOPIC_SET           "ceiling_display/set"
#define MQTT_TOPIC_BRIGHTNESS    "ceiling_display/ch_brightness"
#define MQTT_TOPIC_SAVE_IMAGE    "ceiling_display/save_image"
#define MQTT_TOPIC_LOAD_IMAGE    "ceiling_display/load_image"

const char* ssid = "Kocherga";
const char* password = "Wittgenstein";
const char* mqtt_server = "192.168.88.145";

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GBR + NEO_KHZ800);

WiFiClient espClient;
PubSubClient client(espClient);

//#define DEBUG

#define S_LOG
#define S_LOGLN
#define S_LOGF
#define S_FLUSH

#ifdef DEBUG
#define S_LOG             Serial.print
#define S_LOGLN           Serial.println
#define S_LOGF            Serial.printf
#define S_FLUSH           Serial.flush()
#endif

void setpx(unsigned int i, unsigned int r, unsigned int g, unsigned int b) {
  pixels.setPixelColor(i, r, g, b);
}

void refresh(){
  pixels.show();
}

void load_image(byte* buffer, int length){

  if (buffer[0] == 0) {
    // just plain and simple image buffer.
    if (length != NUMPIXELS * 3 + 1) return;

    byte* off = buffer + 1;
    for (int i=0; i<NUMPIXELS; i++) {
      setpx(i, off[i * 3 + 0], off[i * 3 + 1], off[i * 3 + 2]);
    }
  }
  if (buffer[0] == 1) {

    // palette data with simple compression
    // 1b prefix 1b palette len (<257 * 3)b pallete <NUMPIXELSb data
    // palette size is never > 256 colors

    if (length < 2) return;
    int c_palette = buffer[1] + 1;
    byte* palette = buffer + 2;
    int s_palette = c_palette * 3;
    byte* data = palette + s_palette;
    int s_data = buffer + length - data;

    int pixel = 0;
    for (int i=0; i < s_data; i++) {
      int code = data[i];
      int color = code;
      int repeat = 1;

      if (code == 0) {
        // repetition

        // phhh, stop
        if (i + 2 >= length) return;

        repeat = data[++i];
        color = data[++i];
      }

      // wrong pointer
      if (color > c_palette) return;
      // no
      if (repeat + pixel > NUMPIXELS) return;
      // why?
      if (repeat == 0) return;

      color--;
      for (int j = 0; j < repeat; j++) {
        setpx(pixel++,
          palette[color * 3 + 0],
          palette[color * 3 + 1],
          palette[color * 3 + 2]
        );
      }

    }

  }

}

void callback(char* topic, byte* message, unsigned int length) {
  
  if (strcmp(MQTT_TOPIC_SET, topic) == 0) {
      load_image(message, length);
      refresh();
      return;
  }

  S_LOGF("RX >> [%d] %s \n", length, topic);
  if (strcmp(MQTT_TOPIC_BRIGHTNESS, topic) == 0 && length == 1) {
      pixels.setBrightness(message[0]);
      S_LOGF("   >> changing brightness to [%d]\n", message[0]);
      refresh();
  } else {
      S_LOGLN("   >> no matching commands");
  }

  S_FLUSH;
}

void setup_wifi() {
    S_LOG("wifi -> connecting");
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);

    int retries = 100;
    while (WiFi.status() != WL_CONNECTED && retries) {
        delay(100);
        S_LOG
      (".");
        retries--;
    }

    if (!retries){
      S_LOGLN("wifi -> failed to connect, reseting board.");
      S_FLUSH;
      ESP.reset();
    }

    S_LOGLN("wifi -> connected");
    S_LOGF("wifi -> IP address: %s\n", WiFi.localIP().toString().c_str());
}

void setup_mqtt() {
    S_LOGLN("mqtt -> setting up client");
    client.setServer(mqtt_server, 1883);
    int retries = 50;
    bool connected;
    S_LOG
  ("mqtt -> connecting");
    do {
      S_LOG
    (".");
      connected = client.connect(MQTT_ID);
      if (!connected) delay(100);
      retries--;
    } while (retries && !connected);

    if (!retries) {
      S_LOGLN("mqtt -> failed to connect to server, reseting board.");
      S_FLUSH;
      ESP.reset();
    }

    S_LOGLN("mqtt -> success ");
    client.setCallback(callback);
    client.subscribe(MQTT_TOPIC);

}

void setup() {
  Serial.begin(DEBUG_BAUD_RATE);
  S_LOGLN("\n\n\n == kch display starting == ");
  pixels.setBrightness(5);
  pixels.begin();

  setup_wifi();
  setup_mqtt();

  S_LOGLN(" == we are ready to crash (onto someone) == ");
}

void loop() {
  client.loop();
  if (WiFi.status() != WL_CONNECTED) {
    S_LOGLN("got disconnected from wifi, trying to reconnect");
    setup_wifi();
  }
  if (!client.connected()) {
    S_LOGLN("got disconnected from mqtt, trying to reconnect");
    setup_mqtt();
  }
}
